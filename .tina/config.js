import { defineConfig } from "tinacms";

// Your hosting provider likely exposes this as an environment variable
const branch = process.env.HEAD || process.env.VERCEL_GIT_COMMIT_REF || "master";

export default defineConfig({
  branch,
  clientId: null, // Get this from tina.io
  token: null, // Get this from tina.io
  build: {
    outputFolder: "admin",
    publicFolder: "static",
  },
  media: {
    tina: {
      mediaRoot: "",
      publicFolder: "static",
    },
  },
  schema: {
    collections: [
      {
        name: "stage",
        label: "Stage",
        path: "content/stage",
        defaultItem: () => {
          return {
            author: "Association",
            slug: "yyyymm-shortslug",
            draft: true,
          }
        },
        fields: [
          {
            type: "string",
            name: "title",
            label: "Nom du stage",
            isTitle: true,
            required: true,
          },
          {
            type: "string",
            name: "description",
            label: "Description",
            required: false,
          },
          {
            type: "string",
            name: "author",
            label: "Auteur",
            required: true,
          },
          {
            type: "string",
            name: "slug",
            label: "Slug",
            required: true,
          },
          {
            type: "string",
            name: "thumbnail",
            label: "Miniature",
            required: false,
          },
          {
            type: "string",
            name: "image",
            label: "Illustration",
            required: false,
          },
          {
            type: "datetime",
            name: "dateStart",
            label: "Date de début de stage",
            component: "text",
            required: true,
          },
          {
            type: 'datetime',
            name: 'dateEnd',
            label: 'Date de fin de stage',
            component: "text",
            required: true,
          },
          {
            type: 'datetime',
            name: 'date',
            label: 'Date de publication',
            required: true,
          },
          {
            type: 'boolean',
            name: 'draft',
            label: 'Brouillon',
            required: true,
          },
          {
            type: 'string',
            name: 'tags',
            label: 'Tags',
            list: true,
          },
          {
            type: "rich-text",
            name: "body",
            label: "Contenu",
            isBody: true,
          },
        ],
      },
    ],
  },
});
