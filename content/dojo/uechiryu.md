+++
title = "Uechi Ryu"
description = "Cours de Uechi Ryu"
slug = "uechiryu"
author = "Association"
thumbnail = ""
image = ""
weight = 3
tags = ["karate"]
draft = false
+++
# Style

D'origine chinoise (basé sur les boxes du tigre, du dragon et de la grue), l'originalité du style Uechi Ryu réside dans le travail d'attaques réalisées avec la main ouverte (piques de doigts, tranchants, paumes…), ainsi que dans l'alternance des principes « souple » et « dur ». Des exercices d'endurcissement corporel et l'utilisation de matériel spécifique d'origine de l'île d'Okinawa viennent compléter cet arsenal technique.

Dès sa création , le style a placé le combat au centre de sa pratique. Les katas n'ont pas subi les dérives esthétiques liées à la compétition sportive. De la même manière, l'exercice du combat au dojo (pour les adultes) n'a pas pour objectif une victoire arbitrée mais reste bien lié aux origines de l'art martial : le combat réel de survie.

C'est dans cette recherche constante de la « vérité martiale » qu'est né le groupe KOTEIKAI, sous l'impulsion des frères PEREZ et de certains des élèves de Takemi TAKAYASU dont notre professeur, Christophe POULAIN.

Tout en respectant scrupuleusement l'héritage de l'école Uechi, le programme UECHI RYU EVOLUTION poursuit la recherche d'efficacité caractéristique du style en proposant d'ouvrir le champ des situations proposées. Lutte debout, combat au sol, techniques contre armes, combat contre plusieurs adversaires sont ainsi abordés.

# Cours

Cours collectifs (et individuels à la demande), stages mensuels, adultes et enfants, tout niveau accepté (débutant à expert) de : Kempo Karaté Uechi Ryu (enfants et adultes), combat libre (à partir de 14 ans).
