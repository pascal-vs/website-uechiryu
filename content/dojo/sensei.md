+++
title = "Sensei Christophe Poulain"
description = "Historique de pratique"
slug = "sensei"
author = "Association"
thumbnail = ""
image = ""
weight = 2
tags = [""]
draft = false
+++

Christophe Poulain, né le 14 juin 1972, pratique les Arts Martiaux depuis 36 ans.

Il débute le Judo et le Kendo en 1987. En 1990 il découvre le Karate Shotokan et la boxe Thaïlandaise.

Il pratique le Tai Chi Chuan et l'Aïkido.

En 1998 il rencontre le grand maître TAKEMI TAKAYASU 9ème Dan de l'école Uechi Ryu Karate Do Kenyukai d'Okinawa et devient son élève et obtient son diplôme d'instructeur en Uechi Ryu en 2002. Il restera aurpès de son Maître jusqu'à la fin de son enseignement.

Aujourd'hui il est un de ses fidèles représentants en France au sein du groupe créé en son honneur : la KOTEIKAI.

Il découvre les Arts Martiaux Philippins avec le style Sayoc Kali en 2009.

Rencontre avec Maul Mornie du SSBD - Silat Suffian Bela Diri de Brunei.

En 2023, il est :
* 2ème Dan de Judo
* 3ème Dan de Jiu Jitsu
* 3ème Dan de Shotokan
* 6ème Dan de Uechi Ryu

BEES (Brevet d'État d'Éducateur Sportif) 1er degré, instructeur de Uechi Ryu diplômé par Sensei Takayasu Takemi, enseigne également la Self Defense.

Pratique le Silat SSBD, le Kali et le Iai Do.

En 2022, il devient référent Uechi Ryu au sein de la Fédération WUKF (World Union Karate-Do Federations) France.
