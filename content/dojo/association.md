+++
title = "Association"
description = "Historique de l'association"
slug = "association"
author = "Association"
thumbnail = ""
image = ""
weight = 1
tags = [""]
draft = false
+++

# Histoire

Création de notre association de type loi 1901 en 1999 sous le nom de "Kokoro Shin Do". Notre objectif était la promotion et la diffusion des Arts Martiaux Traditionnels asiatiques.

En 2002 notre association prend le nom "Uechi Ryu Karate Do Kenyukai Picardie" en rapport avec le rattachement au groupe de notre Maître : Sensei TAKEMI TAKAYASU 9ème Dan Hanshi de Uechi Ryu Kenyukai d'Okinawa.

Autour de 2005 Maître Takayasu cesse son enseignement. En son honneur le groupe "Koteikai", qui réunit certains de ses plus anciens élèves, est créé afin de perpétuer son enseignement le plus justement possible. À la même période, afin de mieux définir leurs pratique, les mêmes personnes sous l'influence de Shihan Raphaël Perez se réunissent sous la bannière du "Uechi Ryu Evolution".

Aujourd'hui dans notre dojo rue Béranger nous dispensons des cours de :
* Uechi Ryu
* Self Defense, dans ce cours nous étudions le Silat, les FMA (Filipino Martial Arts), le Jiu Jitsu, ... et tout ce qui peut nous apporter quelque chose dans le cadre de cette étude.

Nous organisons des stages de Self Defense et d'Arts Martiaux régulièrement avec des experts comptant dans ces disciplines (Silat, Kali, Kobudo, Karate, Iaido, etc.)

# Partenariats publics

* AMIENS METROPOLE avec l'OSAM et les mairies de quartiers
* collectivités territoriales
* subvention direction régionale de la jeunesse, des sports et de la cohésion sociale

# Partenariats privés

Autres structures associatives notamment implantées dans les quartiers d'Amiens Métropole telles que le Centre ALCO.

# Cours

Les cours sont assurés au sein de notre Dojo tout équipé situé en centre-ville mais aussi à l'extérieur selon vos besoins (centres aérés, associations de quartier, stages pour comités d’entreprise, <i>etc</i>).
