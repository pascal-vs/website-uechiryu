+++
title = "Yoga"
description = "Cours de Yoga / Pilates"
slug = "yoga"
author = "Association"
thumbnail = ""
image = ""
weight = 15
tags = ['yoga']
draft = true
+++
# Présentation

Chloé Quignon (https://www.facebook.com/tonicopilates) est votre enseignante pour ces cours.

# Horaires

Tous les Mardis :
* 18h00 : pilates
* 19h10 : yoga

# Tarifs

Voir directement avec Chloé.
