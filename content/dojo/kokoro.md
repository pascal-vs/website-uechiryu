+++
title = "Atelier Kokoro"
description = "Sport Santé"
slug = "atelier-kokoro"
author = "Association"
thumbnail = ""
image = ""
weight = 7
tags = ["santé"]
draft = false
+++

L'atelier Kokoro rassemble des activités douces pour rester en bonne santé, éviter ou lutter contre des problèmes médicaux. Un mélange unique d'arts martiaux et de pratiques de relaxation traditionnelle afin d'équilibrer corps et esprit au service de votre bien-être quotidien.
