+++
title = "Stage de Uechi Ryu avec les frères Perez"
description = "Uechi Ryu Evolution"
slug = "20240203-uechi"
author = "Pascal"
thumbnail = "images/stages/20240203_uechi/20240203_uechi-banner.jpg"
image = "images/stages/20240203_uechi/20240203_uechi-banner.jpg"
dateStart = "2024-02-03T10:30:00"
dateEnd = "2024-02-03T13:30:00"
date = "2024-01-15"
tags = ["karate", "stage"]
gallery = false
draft = false
+++

### Programme

Stage du groupe Uechi Ryu Evolution, avec Raphaël Perez et Vincent Perez accompagnés de leurs élèves.

### Infos utiles

Tarif de 30 €.

Informations complémentaires et réservations au 06 14 41 16 00.

Au Dojo de l'association, Uechi Ryu Budokan : 20 rue Béranger à Amiens
