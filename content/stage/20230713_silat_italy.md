+++
title = "Summer Camp - Officina Del Silat"
description = "Summer Camp dirigé par Carlo Andreis de Officina Del Silat"
slug = "20230713-silat-italy"
author = "Pascal"
thumbnail = "images/stages/20230719_silat_italy/officina_del_silat_logo.jpg"
image = "images/stages/20230719_silat_italy/horizontal.jpg"
dateStart = "2023-07-13"
dateEnd = "2023-07-16"
date = "2023-08-23"
tags = ["silat", "stage", "italy", "camp"]
gallery = true
draft = false
+++

### Stage Silat

Christophe Poulain, notre sensei, a participé au camp d'été international dirigé par Carlo Andreis en Italie.

Après avoir reçu Carlo au cours de l'année, Christophe a eu le plaisir d'assister à son camp d'été en Italie !

Plus d'infos sur la page Facebook de l'Officina del Silat : https://www.facebook.com/officinadelsilat.

Après des années d'étude de ce style, Christophe ouvre enfin une section Silat à la rentrée 2023 !
