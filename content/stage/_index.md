+++
title = "Stages au Dojo"
+++

Toute l'année des stages sont également proposés en collaboration avec d'autres associations, la WUKF France, des experts nationaux et internationaux (entre autres : Silat avec l'Officina del Silat de Carlo Andreis, un des premiers élèves de Maul Mornie, PARIS XX UECHI RYU avec les frères PEREZ élèves de Takemi TAKAYASU).
