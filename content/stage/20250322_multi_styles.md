+++
title = "Stage multi disciplines"
description = "Stage découverte à Râches"
slug = "20250322_multi_styles"
author = "Pascal"
thumbnail = "images/stages/20250322_multi_styles/vertical.jpg"
image = "images/stages/20250322_multi_styles/vertical.jpg"
dateStart = "2025-03-22T14:00:00"
dateEnd = "2025-03-23T10:30:00"
date = "2025-01-27"
tags = ["karate", "stage", "uechi", "kyokushinkai", "krav maga", "laojia tai chi"]
gallery = false
draft = false
+++

### Programme

Dans l'ordre chronologique

* Kyokushinkai par Jérôme De Timmerman
* Krav Maga par Pascal Adam
* Uechi Ryu par Christophe Poulain
* LaoJia Tai Chi par Christian Declercq

### Infos utiles

Samedi 22 mars de 14h00 à 17h30 et dimanche 23 mars de 9h à 10h30.

PAF : gratuit pour les licenciés WUKF et Fameda, 20 € pour les autres

[À la salle des sports, rue des écoles à Râches](https://maps.app.goo.gl/wcGdykMA4iqG6Mq96)
