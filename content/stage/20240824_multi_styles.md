+++
title = "Stage multi styles"
description = "Stage découverte à Friville-Escarbotin"
slug = "20240824_multi_styles"
author = "Pascal"
thumbnail = "images/stages/20240824_multi_styles/poster_small.jpg"
image = "images/stages/20240824_multi_styles/poster.png"
dateStart = "2024-08-24T09:00:00"
dateEnd = "2024-08-24T17:00:00"
date = "2024-08-09"
tags = ["karate", "stage", "uechi", "kendo", "iaido", "wado"]
gallery = false
draft = false
+++

### Programme

* Uechi Ryu par Christophe Poulain
* Kendo par Stéphane Merigout
* Iaido par Cédric Despagne
* Wado Ryu par Joffrey Gallet

### Infos utiles

Samedi 24 août de 9h00 à 17h00 (prévoir un pique-nique).

Ramenez votre bokken/iaito.

Contact: Stéphane Merigout, kidokan@orange.fr

[Au Dojo Kidokan à Friville-Escarbotin](https://maps.app.goo.gl/1mGD8Bh1ehAd2BxL7)
