+++
title = "Saison 2024 - 2025"
image_schedule = ""
[config_schedule.1]
  name = "Lundi"
  # [[config_schedule.1.lessons]]
  #   name = "Sport Santé"
  #   from = "18h"
  #   to = "19h"
  [[config_schedule.1.lessons]]
    name = "Uechi Ryu (adultes)"
    from = "19h"
    to = "20h30"
    info = "Débutants"
  [[config_schedule.1.lessons]]
    name = "Iai"
    from = "20h"
    to = "21h"
    info = "(dernier lundi du mois)"
[config_schedule.2]
  name = "Mardi"
  # [[config_schedule.2.lessons]]
  #   name = "Uechi Ryu (enfants)"
  #   from = "17h30"
  #   to = "18h30"
  #   info = "à Fréchencourt, pas au dojo habituel"
  [[config_schedule.2.lessons]]
    name = "Self Defense (Officina\u00A0del\u00A0Silat, FMA)"
    from = "18h30"
    to = "19h30"
  [[config_schedule.2.lessons]]
    name = "Uechi Ryu (adultes)"
    from = "19h30"
    to = "21h"
[config_schedule.3]
  name = "Mercredi"
  [[config_schedule.3.lessons]]
    name = "Uechi Ryu (enfants)"
    from = "11h"
    to = "12h"
  [[config_schedule.3.lessons]]
    name = "Tai Chi"
    from = "12h30"
    to = "13h30"
  [[config_schedule.3.lessons]]
    name = "Uechi Ryu (ados)"
    from = "14h"
    to = "15h"
  [[config_schedule.3.lessons]]
    name = "Musculation Renforcement"
    from = "18h"
    to = "19h30"
# [config_schedule.4]
#   name = "Jeudi"
#   [[config_schedule.4.lessons]]
#     name = "Self Defense"
#     from = "18h30"
#     to = "20h"
[config_schedule.5]
  name = "Vendredi"
 [[config_schedule.5.lessons]]
   name = "Tai Chi"
   from = "11h"
   to = "12h"
  # [[config_schedule.5.lessons]]
  #   name = "Sport Santé"
  #   from = "18h"
  #   to = "19h"
  [[config_schedule.5.lessons]]
    name = "Self Defense (Officina\u00A0del\u00A0Silat, FMA)"
    from = "19h"
    to = "20h30"
[config_schedule.6]
  name = "Samedi"
  [[config_schedule.6.lessons]]
    name = "Uechi Ryu (enfants)"
    from = "11h"
    to = "12h"
  [[config_schedule.6.lessons]]
    name = "Uechi Ryu (adultes)"
    from = "13h30"
    to = "15h30"
+++

## Tarifs

Pour tous les cours, il est bien sûr possible de venir faire un essai gratuit.

N'hésitez pas à nous appeler pour avoir plus d'informations (voir [section "contact"](#contact))

# Majeurs

- 300 € pour 1 activité / an
- \+ 50 € par activité supplémentaire
- \+ Licence WUKF : 28 €

# Mineurs

- 250 € / an
- \+ Licence WUKF : 20 €

# Tarif réduit

Tarif réduit pour les étudiants, retraités, demandeurs d'emploi, inscription famille (à partir de 2 membres) sur présentation d'un justificatif.

# Adhésion

Adhésion annuelle à l'association : 10 €

# Infos utiles !

L'association accepte les chèques collège et les pass'Sport, possibilité de payer via Helloasso.
