+++
title = "Nouvel atelier Kokoro"
description = "Sport Santé tous les mercredis"
slug = "20240412-atelier-kokoro"
author = "Pascal"
thumbnail = "images/articles/20240412-atelier-kokoro/thumbnail.jpg"
image = "images/articles/20240412-atelier-kokoro/image.jpg"
date = "2024-04-12"
tags = ["santé"]
draft = false
+++

Nouveauté ! 🎊

Dans le cadre de nos actions sport santé, nous sommes ravis de vous présenter notre nouvel atelier "Kokoro" 💙.

À la recherche d'une activité douce qui vous permette de rester en bonne santé ? Pour vous éviter ou lutter contre des problèmes médicaux 🩺 ? Nous vous proposons un mélange unique d'arts martiaux et de pratiques de relaxation traditionnelle afin d'équilibrer corps et esprit au service de votre bien-être quotidien 🧘.

Si vous vous inscrivez après avoir été accompagné par la Maison Sport Santé Amiens Métropole, 50 euros seront pris en charge sur votre cotisation 🙂.
