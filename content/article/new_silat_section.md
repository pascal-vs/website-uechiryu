+++
title = "Section Silat à la rentrée 2023"
description = "Ouverture d'une section de Silat"
slug = "new-silat-section"
author = "Pascal"
thumbnail = ""
image = ""
date = "2023-08-23"
tags = ["silat"]
draft = false
+++

### Nouvel art martial proposé à la rentrée

Bonne nouvelle : ouverture de la section Silat à la rentrée 2023 !

Chrisotphe Poulain étudie le style de Maul Mornie (SSBD) depuis des années. Après sa rencontre avec Carlo Andreis, un des premiers élèves de Mornie en Europe, l'ouverture d'une section "Officina del Silat" a pu être possible.
