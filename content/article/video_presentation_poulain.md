+++
title = "Présentation vidéo de Christophe Poulain"
description = "Présentation vidéo de Christophe Poulain par Uechi Ryu Evolution"
slug = "video-presentation-poulain"
author = "Pascal"
thumbnail = ""
image = ""
date = "2023-08-23"
tags = ["karate"]
draft = false
+++

Vidéo de présentation de Christophe Poulain par le groupe Uechi Ryu Evolution [ici sur Youtube](https://www.youtube.com/watch?v=ezp8M-3AaoM)

<iframe width="560" height="315" src="https://www.youtube.com/embed/ezp8M-3AaoM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
