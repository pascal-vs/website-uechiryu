+++
title = "Japan Manga Wave"
description = "Participation à l'événement Japan Manga Wave"
slug = "20231118-japanmangawave"
author = "Pascal"
thumbnail = "images/articles/20231118-japanmangawave/japan_manga_wave_img_small.png"
image = "images/articles/20231118-japanmangawave/japan_manga_wave_img.png"
dateStart = "2023-11-18T09:30:00"
dateEnd = "2023-11-19T19:00:00"
date = "2023-11-11"
tags = ["uechi"]
gallery = true
draft = false
+++

Retour en images sur la [Amiens - Japan Manga Wave](https://japanmangawave.com/) ([lien vers leur page facebook](https://www.facebook.com/AmiensJMW)) où nous étions présents les 18 et 19 novembre derniers.

Merci à toutes et tous d'être venus passer un moment avec nous pour échanger, taper dans les paos ou juste fracasser Bob.

Un grand merci à Cécile et Nathalie pour les photos & vidéos.
