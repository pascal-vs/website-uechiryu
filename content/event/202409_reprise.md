+++
title = "Reprise des cours"
slug = "2024-reprise"
author = "Pascal"
dateStart = "2024-09-03"
dateEnd = ""
date = "2024-08-26"
draft = false
+++
Nous avons tous hâte de reprendre &#x1F642; !

### Reprise des cours

#### Adultes

Mardi 3 septembre.

#### Enfants

Mercredi 11 septembre.

### Nouveautés

* Section Tai Chi
* Cours adolescents pour le Uechi Ryu
* La section Silat de l'an dernier se transforme en section self defense avec des cours de self, Silat et FMA cette année.

### Horaires

Les [horaires et tarifs]({{< ref "/info" >}}) sont à jour !
