+++
title = "Reprise des cours"
slug = "2023-reprise"
author = "Pascal"
dateStart = "2023-09-05"
dateEnd = ""
date = "2023-08-23"
draft = false
+++
Nous avons tous hâte de reprendre &#x1F642; !

### Karaté

#### Adultes

Mardi 5 septembre à 19h.

#### Enfants

Mercredi 6 septembre à 11h.

### Nouveauté

Rappel : une section Silat va commencer cette année.

### Horaires

La mise à jour des [horaires]({{< ref "/info" >}}) suivra dans le courant du mois de septembre ! Pour le moment pas de changement dans l'immédiat.
