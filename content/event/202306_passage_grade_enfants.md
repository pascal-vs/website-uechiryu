+++
title = "Passage de grades enfants"
slug = "202306-passage-grade-enfants"
author = "Pascal"
dateStart = "2023-06-21"
dateEnd = "2023-06-21"
date = "2023-06-21"
gallery = true
draft = false
+++
Bravo à tous ! &#x1F44F;

Grades obtenus avec succès, fruit d'un travail sérieux et régulier par tous les élèves &#x1F4AA;.