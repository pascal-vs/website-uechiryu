+++
title = "Financement participatif"
description = "Rénover et améliorer notre dojo"
slug = "20240610_crowdfunding"
author = "Pascal"
dateStart = "2024-06-08"
dateEnd = "2024-07-06"
date = "2024-06-15"
draft = false
+++

Notre association a lancé une campagne de financement participatif pour que vous puissiez nous aider à rénover et améliorer notre dojo.

Un grand merci d'avance pour l'intérêt que vous portez à notre [projet](https://www.helloasso.com/associations/uechi-ryu-kempo-karate/collectes/aidez-nous-a-renover-et-ameliorer-notre-dojo) &#x1F647; .

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/uechi-ryu-kempo-karate/collectes/aidez-nous-a-renover-et-ameliorer-notre-dojo/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>
