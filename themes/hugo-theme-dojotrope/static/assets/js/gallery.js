jQuery(document).ready(function () {
    var images = [];
    const url = `https://www.googleapis.com/storage/v1/b/${hugoBucket}/o?prefix=${hugoSlug}/${hugoImages}`;

    fetch(url)
        .then(response => response.json())
        .then(data => {
            const items = data.items;
            items.forEach(item => {
                if (item.name.slice(-1) != "/") {
                    var name = item.name.substring(item.name.lastIndexOf('/') + 1);
                    var item = {
                        src: hugoImages + '/' + name,
                        srct: hugoThumbnails + '/' + name,
                        title: ""
                    };
                    images.push(item);
                }
            });
            jQuery("#gallery").nanogallery2({
                items:           images,
                thumbnailWidth:  'auto',
                thumbnailHeight: hugoHeight,
                itemsBaseURL:    'https://storage.googleapis.com/' + hugoBucket + '/' + hugoSlug + '/',
                locationHash:    false
            });
        });
});
