# Hugo Theme Dojotrope

Theme based on [Dopetrope](https://html5up.net/dopetrope) from [HTML5 UP](https://html5up.net/) ported to the [Hugo static site generator](https://gohugo.io/) by [Curtis Timson](https://curtiscode.dev).

This is a WIP theme!
